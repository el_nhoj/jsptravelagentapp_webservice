package test;

import model.soa.BookingConfirmationAccommodation;
import model.soa.DataBindingUtils;
import stubs.AccommodationServiceStub;
import stubs.AccommodationServiceStub.Accommodation;
import stubs.AccommodationServiceStub.Customer;

public class DataBinderTest {
	public static void main(String[] args) {
		AccommodationServiceStub.BookingConfirmationAccommodation stubBca = new AccommodationServiceStub.BookingConfirmationAccommodation();
		Customer c = new Customer();
		c.setName("kisou");
		stubBca.setCustomer(c);
		stubBca.setId("stub.id");
		Accommodation accomodation = new Accommodation();
		accomodation.setName("accname");
		stubBca.setAccommodation(accomodation);
		BookingConfirmationAccommodation bca = DataBindingUtils
				.toSOABookingConfirmationAccommodation(stubBca);
		System.out.println(bca.getId());
		
		
	}
}
