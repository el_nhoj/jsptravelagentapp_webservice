package model.beans;

public class BookingConfirmationAccommodation {
	private String id;
	protected Customer customer;
	private Accommodation accommodation;

	public BookingConfirmationAccommodation(Accommodation accommodation,
			Customer customer) {
		this.accommodation = accommodation;
		this.customer = customer;
	}

	public BookingConfirmationAccommodation() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Accommodation getAccommodation() {
		return accommodation;
	}

	public void setAccommodation(Accommodation accommodation) {
		this.accommodation = accommodation;
	}

	public String toString() {
		return getCustomer().getName() + ":" + getAccommodation().getId()
				+ getAccommodation().getName() + ":"
				+ getAccommodation().getAddress() + ":"
				+ getAccommodation().getPhoneNumber() + ":"
				+ getAccommodation().getLocation();
	}
}
