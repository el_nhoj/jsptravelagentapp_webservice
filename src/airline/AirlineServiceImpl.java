package airline;

import interfaces.AirlineService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import stubs.PaymentServiceStub;
import model.beans.BookingConfirmationAirline;
import model.beans.BookingConfirmationOrder;
import model.beans.Customer;
import model.beans.Flight;
import model.beans.TravelAgent;

public class AirlineServiceImpl implements AirlineService {
	private Map<String, String> accounts;
	private Flight[] flightList;
	private final Logger logger = Logger.getLogger("dbConnection");
	// private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	private String driver = "org.apache.derby.jdbc.ClientDriver";
	// private String protocol = "jdbc:derby:";
	private String protocol = "jdbc:derby://localhost:1527/";
	// private String dbName = "C:/Users/John/MyDB";
	// private Properties props;
	private Connection con;
	private Statement st;
	private PaymentServiceStub payStub;

	public AirlineServiceImpl() {
		// Initialize HashMap of TravelAgent account key/values
		accounts = new HashMap<>();
		accounts.put("SOA Travel", "123");
		accounts.put("Cheap Travel", "456");
		accounts.put("Travel Service Inc", "789");

		// Initialize flightList array
		flightList = new Flight[4];

		// // Connection properties
		// props = new Properties();
		// props.put("user", "test");
		// // Set the user value to the userid you have to the database
		// props.put("password", "test");

		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			// Create connection
			// con = DriverManager.getConnection(protocol + dbName
			// + ";create=true", props);

			con = DriverManager.getConnection(protocol + "MyDB;create=true");

			st = con.createStatement();

			// Query and display results
			ResultSet rs = st.executeQuery("SELECT * FROM Flight");
			logger.info("*** Query results:");
			int n = 0;
			while (rs.next()) {
				flightList[n] = new Flight();
				flightList[n].setId(rs.getString(1).toString());
				flightList[n].setAirlineName(rs.getString(2).toString());
				flightList[n].setDepartureDate(rs.getDate(3));
				flightList[n].setDepartureGate(rs.getString(4).toString());
				flightList[n].setDestination(rs.getString(5).toString());
				flightList[n].setCost(rs.getDouble(6));
				n++;
			}

			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception exception) {
			logger.info("Error" + exception.toString());
		}
	}

	@Override
	public TravelAgent login(String username, String password) {
		String expectedPassword = accounts.get(username);
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return TravelAgent object with
			// assigned values
		} else if (expectedPassword.equals(password)) {
			String id;
			if (username.equals("SOA Travel")) {
				id = "00000001";
			} else if (username.equals("Cheap Travel")) {
				id = "00000002";
			} else {
				id = "00000003";
			}
			return new TravelAgent(id, username, password);
		} else {
			// If username good, but bad password
			return null;
		}
	}

	@Override
	public Flight[] getFlights() {
		return flightList;
	}

	// Legacy method
	public BookingConfirmationAirline bookFlight(TravelAgent travelAgent,
			Flight flight, Customer customer) {
		String expectedPassword = accounts.get(travelAgent.getName());
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return
			// BookingConfirmationAirline object
		} else if (expectedPassword.equals(travelAgent.getPassword())) {
			BookingConfirmationAirline airBooking = new BookingConfirmationAirline(
					flight, customer);
			airBooking.setId(UUID.randomUUID().toString());
			return airBooking;
		} else {
			// If username good but bad password
			return null;
		}
	}

	@Override
	public BookingConfirmationOrder confirmBooking(Customer customer,
			double total, String cartContents) {

		BookingConfirmationOrder bookingOrder = null;

		// Attempt to validate cost of flights against customer's balance
		try {
			payStub = new PaymentServiceStub(
					"http://localhost:8080/axis2/services/PaymentService");

			// If balance is valid
			if (payStub.checkBalance(customer.getUsername(),
					customer.getBalance(), total)) {

				// Create booking and generate ID
				bookingOrder = new BookingConfirmationOrder(UUID.randomUUID()
						.toString(), cartContents, customer.getId());

				// Write booking to database
				try {
					// Instantiate driver
					Class.forName(driver).newInstance();

					con = DriverManager.getConnection(protocol
							+ "MyDB;create=true");

					st = con.createStatement();
					st.executeUpdate("INSERT INTO BookingConfirmationOrder VALUES ('"
							+ bookingOrder.getId()
							+ "', '"
							+ bookingOrder.getCartContents()
							+ "', '"
							+ bookingOrder.getCustomerId() + "')");
				} catch (Exception e) {
					logger.info("Error" + e.toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bookingOrder;
	}

	// Create a Customer object method
	public Customer createCustomer(String name, String username,
			String password, String address, double balance) {
		String customerId = UUID.randomUUID().toString();
		return new Customer(customerId, name, username, password, address,
				balance);
	}
}
