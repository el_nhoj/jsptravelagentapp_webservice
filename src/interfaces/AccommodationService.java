package interfaces;

import model.beans.Accommodation;
import model.beans.BookingConfirmationAccommodation;
import model.beans.Customer;
import model.beans.TravelAgent;

public interface AccommodationService {
	/**
	 * @param username
	 *            the travel agent accessing the service
	 * @param password
	 *            the travel agent accessing the service
	 * @return a Travel Agent token that can be used for authenticating to the
	 *         service
	 */
	public abstract TravelAgent login(String username, String password);

	/**
	 * @return an collection of Accommodation options to display to the user for
	 *         purchase
	 */
	public model.beans.Accommodation[] getAccommodationList(String destination);

	/**
	 * @param travelAgent
	 *            the travel agent accessing the service
	 * @param accommodation
	 *            the accommodation their customer wants to book
	 * @param customer
	 *            the customer that will be traveling
	 * @return a booking confirmation slip with details of the flight
	 */
	public abstract BookingConfirmationAccommodation bookAccommodation(
			TravelAgent travelAgent, Accommodation accommodation,
			Customer customer);
}