package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.Properties;

import org.apache.log4j.Logger;

public class DatabaseInitialization {

	public static void main(String[] args) throws Exception {
		DatabaseInitialization appDB = new DatabaseInitialization();
		appDB.createAndPopulate();
	}

	// Method to initialize and populate application database
	public void createAndPopulate() {

		// Use log4j for simple logging
		final Logger logger = Logger.getLogger("dbConnection");

		// Put DB operations in separate thread
		new Thread(new Runnable() {
			public void run() {
				// Set driver and user properties
				String driver = "org.apache.derby.jdbc.ClientDriver";

				String protocol = "jdbc:derby://localhost:1527/";
				// String dbName = "C:/Users/John/MyDB";
				// User connection properties
				// Properties props = new Properties();
				// props.put("user", "test");
				// props.put("password", "test");

				try {
					// Instantiate the driver
					Class.forName(driver).newInstance();

					// Create a connection
					// Connection con = DriverManager.getConnection(protocol
					// + dbName + ";create=true", props);
					Connection con = DriverManager.getConnection(protocol
							+ "MyDB;create=true");
					Statement st = con.createStatement();

					// Drop tables if they already exist
					st.executeUpdate("DROP TABLE Customer");
					logger.info("*** Deleted TABLE: Customer");
					st.executeUpdate("DROP TABLE Flight");
					logger.info("*** Deleted TABLE: Flight");
					st.executeUpdate("DROP TABLE Accommodation");
					logger.info("*** Deleted TABLE: Accommodation");
					 st.executeUpdate("DROP TABLE BookingConfirmationOrder");
					 logger.info("*** Deleted TABLE: BookingConfirmationOrder");

					// Create table Customer
					st.executeUpdate("CREATE TABLE Customer (id VARCHAR(15) NOT NULL, name VARCHAR(70) NOT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(128) NOT NULL, address VARCHAR(70), balance FLOAT, PRIMARY KEY (id))");
					logger.info("*** Created table: Customer");

					// Add records to Customer
					st.executeUpdate("INSERT INTO Customer VALUES ('CUST001','John Smith', 'jsmith', 'test123', '13 Bell St, Melbourne', 4000.00)");
					st.executeUpdate("INSERT INTO Customer VALUES ('CUST002','Robert Redford', 'rredford', 'test456', '48 Billing St, Springvale', 1500.00)");
					st.executeUpdate("INSERT INTO Customer VALUES ('CUST003','Tom Jones', 'tjones', 'test789', '121 Baker Road, Keysborough', 250.00)");
					logger.info("*** Inserted default records: Customer");

					// Create table Flight
					st.executeUpdate("CREATE TABLE Flight (id VARCHAR(15) NOT NULL, airlineName VARCHAR(70), departureDate DATE, departureGate INTEGER, destination VARCHAR(70), cost FLOAT, PRIMARY KEY (id))");
					logger.info("*** Created table: Flight");

					// Add records to Flight
					st.executeUpdate("INSERT INTO Flight VALUES ('QF123', 'QANTAS', '2015-06-25', 9, 'Broome', 1000.0)");
					st.executeUpdate("INSERT INTO Flight VALUES ('EMM456', 'Emirates', '2015-06-23', 5, 'Dubai', 1200.0)");
					st.executeUpdate("INSERT INTO Flight VALUES ('SIN764', 'Singapore', '2015-06-29', 3, 'Singapore', 5000.0)");
					st.executeUpdate("INSERT INTO Flight VALUES ('THA999', 'Thai Airways', '2015-06-30', 1, 'Bangkok', 500.0)");
					logger.info("*** Inserted default records: Flight");

					// Create table Accommodation
					st.executeUpdate("CREATE TABLE Accommodation (id VARCHAR(40) NOT NULL, name VARCHAR(70), address VARCHAR(70), phoneNumber VARCHAR(35), location VARCHAR(70), cost FLOAT, PRIMARY KEY (id))");
					logger.info("*** Created table: Accommodation");

					// Add records to Accommodation
					st.executeUpdate("INSERT INTO Accommodation VALUES('64dbb281-3677-4089-a7e0-9c64a2fe3f14', 'Cable Beach Resort', '1 Cable Beach Road | Cable Beach, Broome, Western Australia 6726', '(08) 9192 0400', 'Broome', 150.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('a56039ad-8f52-4044-af31-9d2f4e596a4f', 'Mantra Frangipani', '15 Millington Drive | Cable Beach, Broome, Western Australia 6726', '(08) 9192 0401', 'Broome', 125.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('81ce6f6c-9118-41ed-a72b-2984eee145de', 'Bali Hai Resort & Spa', '6 Murray Rd, Cable Beach, Broome, Western Australia 6726', '(08) 9192 0402', 'Broome', 167.95)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('2db135ef-7848-439a-9672-945c2653f5bd', 'Oaks', '99 Robinson Street, Broome, Western Australia 6725', '(08) 9192 0403', 'Broome', 110.50)");

					st.executeUpdate("INSERT INTO Accommodation VALUES('72a29c92-40b2-4637-a008-3787ee27a33a', 'Atlantis, The Palm', 'Crescent Road | The Palm, Dubai, United Arab Emirates', '0011 971 4 426 0000', 'Dubai', 200.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('6fec1c6d-d6a0-4d43-ad18-02c09fd38c71', 'Radisson Blu Hotel, Dubai Deira Creek', 'Bani Yas Road, Dubai 476, United Arab Emirates', '0011 971 4 222 7171', 'Dubai', 210.55)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('92a17a3c-5487-4519-b3ed-af1af86afc68', 'Shangri-La Hotel, Dubai', 'Sheikh Zayed Road | PO Box 75880, Dubai, United Arab Emirates', '0011 971 4 222 7172', 'Dubai', 190.50)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('53a6e314-2db9-439b-96bb-ce512db2de4d', 'Arabian Courtyard Hotel & Spa', 'Al Fahidi Street, Dubai, United Arab Emirates', '0011 971 4 222 7172', 'Dubai', 155.10)");

					st.executeUpdate("INSERT INTO Accommodation VALUES('74f3a4e8-2c4c-4ea4-8c42-7ea39b31b78f', 'Shangri-La Hotel, Singapore', '22 Orange Grove Rd, Singapore 258350', '0011 65 6505 5663', 'Singapore', 99.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('d1f24ead-b1b8-4a91-9a56-806dbd52a55b', 'InterContinental', '80 Middle Rd, Singapore 188966, Singapore', '0011 65 6505 5664', 'Singapore', 115.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('10ab2142-3925-4492-83ff-403a4f2fe927', 'Carlton Hotel', '76 Bras Basah Rd, Singapore 189558, Singapore', '0011 65 6505 5665', 'Singapore', 155.50)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('1e4c9c0f-e2fb-4a31-b11f-7f1ad28a99c5', 'Park Royal', '7500 Beach Road, Singapore 199591, Singapore', '0011 65 6505 5666', 'Singapore', 160.10)");

					st.executeUpdate("INSERT INTO Accommodation VALUES('8e1e45d5-6f58-43cf-b912-0a696b4a627c', 'Shangri-La Hotel', '89 Soi Wat Suan Plu, New Road | Bangrak, Bangkok 10500, Thailand', '1-800-760-594', 'Bangkok', 125.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('335dfb3b-6723-4f31-a33b-eba8f78273b3', 'Holiday Inn', '1 Sukhumvit 22 | Klongton, Klongtoey, Bangkok 10110, Thailand', '1-800-760-595', 'Bangkok', 85.50)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('49b5208c-b115-426c-8f21-383711f6f5a1', 'Oriental Residence', '110 Wireless Road | Lumpini, Bangkok 10330, Thailand', '1-800-760-593', 'Bangkok', 115.00)");
					st.executeUpdate("INSERT INTO Accommodation VALUES('f5a77cca-82d6-4b34-95c4-642230dd1129', 'Amari Watergate', '847 Phetchaburi Road, Bangkok 10400, Thailand', '1-800-760-592', 'Bangkok', 95.95)");
					logger.info("*** Inserted default records: Accommodation");

					// Create BookingConfirmationOrder table
					st.executeUpdate("CREATE TABLE BookingConfirmationOrder (id VARCHAR(40) NOT NULL, cartContents LONG VARCHAR NOT NULL, customerId VARCHAR(40) NOT NULL, PRIMARY KEY (id))");
					logger.info("*** Created table: BookingConfirmationOrder");

					// Query and display results from each table
					ResultSet rs = st.executeQuery("SELECT * FROM Customer");
					logger.info("*** Query results: Customer");
					while (rs.next()) {
						logger.info("Customer ID: " + rs.getString(1));
						logger.info("Customer Name: " + rs.getString(2));
						logger.info("Customer Username: " + rs.getString(3));
						logger.info("Customer Address: " + rs.getString(5));
						logger.info("Customer Balance: " + rs.getString(6));
					}

					ResultSet rs2 = st.executeQuery("SELECT * FROM Flight");
					logger.info("*** Query results: Flight");
					while (rs2.next()) {
						logger.info("Flight ID: " + rs2.getString(1));
						logger.info("Flight Airline: " + rs2.getString(2));
						logger.info("Flight Departure Date: "
								+ rs2.getString(3));
						logger.info("Flight Departure Gate: "
								+ rs2.getString(4));
						logger.info("Flight Destination: " + rs2.getString(5));
						logger.info("Flight Cost: " + rs2.getString(6));
					}

					ResultSet rs3 = st
							.executeQuery("SELECT * FROM Accommodation");
					logger.info("*** Query results: Accommodation");
					while (rs3.next()) {
						logger.info("Accommodation ID: " + rs3.getString(1));
						logger.info("Accommodation Name: " + rs3.getString(2));
						logger.info("Accommodation Address: "
								+ rs3.getString(3));
						logger.info("Accommodationtomer Phone Number: "
								+ rs3.getString(4));
						logger.info("Accommodationtomer Location: "
								+ rs3.getString(5));
						logger.info("Accommodationtomer Cost: "
								+ rs3.getString(6));
					}

					// Release resources
					rs.close();
					rs2.close();
					rs3.close();
					st.close();
					con.close();

				} catch (SQLException sqlEx) {
					while (sqlEx != null) {
						logger.error("[SQLException] " + "SQLState: "
								+ sqlEx.getSQLState() + ", Message: "
								+ sqlEx.getMessage() + ", Vendor: "
								+ sqlEx.getErrorCode());
						sqlEx = sqlEx.getNextException();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		}).start();
	}

}
