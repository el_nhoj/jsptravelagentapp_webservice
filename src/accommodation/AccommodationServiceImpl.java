package accommodation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
//import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;

import model.beans.Accommodation;
import model.beans.BookingConfirmationAccommodation;
import model.beans.Customer;
import model.beans.TravelAgent;
import interfaces.AccommodationService;

public class AccommodationServiceImpl implements AccommodationService {
	private Map<String, String> accounts;
	private Accommodation[] accommodationList;
	private final Logger logger = Logger.getLogger("dbConnection");
	// private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
	private String driver = "org.apache.derby.jdbc.ClientDriver";
	// private String protocol = "jdbc:derby:";
	private String protocol = "jdbc:derby://localhost:1527/";
	// private String dbName = "C:/Users/John/MyDB";
	// private Properties props;
	private Connection con;
	private Statement st;

	public AccommodationServiceImpl() {
		// Initialize HashMap of TravelAgent account key/values
		accounts = new HashMap<>();
		accounts.put("SOA Travel", "123");
		accounts.put("Cheap Travel", "456");
		accounts.put("Travel Service Inc", "789");

		// // Connection properties
		// props = new Properties();
		// props.put("user", "test");
		// // Set the user value to the userid you have to the database
		// props.put("password", "test");

		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			// Create connection
			// con = DriverManager.getConnection(protocol + dbName
			// + ";create=true", props);

			con = DriverManager.getConnection(protocol + "MyDB;create=true");

			st = con.createStatement();

			// Find out how many rows exist in table
			ResultSet rs = st
					.executeQuery("SELECT COUNT(*) AS rowcount FROM Accommodation");
			rs.next();
			int size = rs.getInt("rowcount");

			// Instantiate array with the row size;
			accommodationList = new Accommodation[size];

			// Create objects in array while there are results
			ResultSet rs2 = st.executeQuery("SELECT * FROM Accommodation");
			int n = 0;
			while (rs2.next()) {
				accommodationList[n] = new Accommodation();
				accommodationList[n].setId(rs2.getString(1).toString());
				accommodationList[n].setName(rs2.getString(2).toString());
				accommodationList[n].setAddress(rs2.getString(3).toString());
				accommodationList[n]
						.setPhoneNumber(rs2.getString(4).toString());
				accommodationList[n].setLocation(rs2.getString(5).toString());
				accommodationList[n].setCost(rs2.getDouble(6));
				n++;
			}

			// Release resources
			rs.close();
			rs2.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
	}

	@Override
	public TravelAgent login(String username, String password) {
		String expectedPassword = accounts.get(username);
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return TravelAgent object with
			// assigned values
		} else if (expectedPassword.equals(password)) {
			String id;
			if (username.equals("SOA Travel")) {
				id = "00000001";
			} else if (username.equals("Cheap Travel")) {
				id = "00000002";
			} else {
				id = "00000003";
			}
			return new TravelAgent(id, username, password);
		} else {
			// If username good, but bad password
			return null;
		}
	}

	@Override
	public model.beans.Accommodation[] getAccommodationList(String destination) {
		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB;create=true");
			st = con.createStatement();

			ResultSet rs = null;
			// Select query and execute based on destination
			switch (destination) {
			case "Broome":
				rs = st.executeQuery("SELECT * FROM Accommodation WHERE location = 'Broome'");
				break;
			case "Dubai":
				rs = st.executeQuery("SELECT * FROM Accommodation WHERE location = 'Dubai'");
				break;
			case "Singapore":
				rs = st.executeQuery("SELECT * FROM Accommodation WHERE location = 'Singapore'");
				break;
			case "Bangkok":
				rs = st.executeQuery("SELECT * FROM Accommodation WHERE location = 'Bangkok'");
				break;
			default:
				break;
			}
			// Initialize and return accommodationList array
			accommodationList = new Accommodation[4];
			int n = 0;
			// Create objects in array while there are results
			while (rs.next()) {
				accommodationList[n] = new Accommodation();
				accommodationList[n].setId(rs.getString(1).toString());
				accommodationList[n].setName(rs.getString(2).toString());
				accommodationList[n].setAddress(rs.getString(3).toString());
				accommodationList[n].setPhoneNumber(rs.getString(4).toString());
				accommodationList[n].setLocation(rs.getString(5).toString());
				accommodationList[n].setCost(rs.getDouble(6));
				n++;
			}

			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception exception) {
			logger.info("Error" + exception.toString());
		}
		return accommodationList;
	}

	// Return all accommodations
	public model.beans.Accommodation[] getAllAccommodationList() {
		return accommodationList;
	}

	@Override
	public BookingConfirmationAccommodation bookAccommodation(
			TravelAgent travelAgent, Accommodation accommodation,
			Customer customer) {
		String expectedPassword = accounts.get(travelAgent.getName());
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return
			// BookingConfirmationAccommodation object
		} else if (expectedPassword.equals(travelAgent.getPassword())) {
			BookingConfirmationAccommodation accommBooking = new BookingConfirmationAccommodation(
					accommodation, customer);
			accommBooking.setId(UUID.randomUUID().toString());
			return accommBooking;
		} else {
			// If username good, but bad password
			return null;
		}
	}

	// Create a Customer object method
	public Customer createCustomer(String name, String username,
			String password, String address, double balance) {
		String customerId = UUID.randomUUID().toString();
		return new Customer(customerId, name, username, password, address,
				balance);
	}
}
